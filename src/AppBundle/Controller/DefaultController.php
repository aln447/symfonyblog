<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\Tools\Pagination\Paginator;
class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        return $this -> redirectToRoute('paged-homepage');
    }
    /**
     * @Route("/page/{page}", name="paged-homepage")
     */
    public function pageAction(Request $request, $page = 1)
    {
        $postPerPage = 8;
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery('SELECT p.idp, p.name, p.data, SUBSTRING(p.text, 1, 150) as text, u.username as author, u.id as authorId FROM AppBundle:Post p LEFT JOIN AppBundle:User u WITH p.ida = u.id ORDER BY p.idp DESC')
            ->setFirstResult( ($page-1) * $postPerPage )
            ->setMaxResults( $postPerPage );

        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..') . DIRECTORY_SEPARATOR,
            'query' => $query->getResult(),
            'page' => $page
        ]);
    }

    /**
     * @Route ("/user/{id}", name="user")
     */
    public function userAction($id)
    {
//        $em = $this->getDoctrine()->getManager();
//        $userQuery = $em->createQuery("SELECT u FROM AppBundle:User u WHERE u.id = {$id}");
//        $postsQuery = $em->createQuery("SELECT p.idp, p.name, p.data, SUBSTRING(p.text, 1, 150) as text FROM AppBundle:Post p WHERE p.ida = {$id}");
//
//        return $this->render('user/user.html.twig', [
//            'user' => $userQuery->getResult(),
//            'posts' => $postsQuery->getResult(),
//            'id' => $id
//        ]);
//
        $em = $this->getDoctrine()->getManager();
        $userQuery = $em->createQuery("SELECT u FROM AppBundle:User u WHERE u.id = {$id}");
        $postsQuery = $em->createQuery("SELECT p.idp, p.name, p.data, SUBSTRING(p.text, 1, 150) as text FROM AppBundle:Post p WHERE p.ida = {$id}");

        return $this->render('user/user.html.twig', [
            'user' => $userQuery -> getResult(),
            'posts' => $postsQuery -> getResult(),
            'id' => $id
        ]);
    }

    /**
     * @Route ("/message/{string}", name="message")
     */
    public function infoAction($string){
        return $this->render('default/message.html.twig', [
            "string" => $string
        ]);
    }

    public function recentCommentsAction ()
    {
        $em = $this->getDoctrine()->getManager();
        $comments = $em->createQuery("
            SELECT c.text, c.postId, u.username, u.id, p.name
            FROM AppBundle:Comment c 
            LEFT JOIN AppBundle:User u 
            WITH c.authorId = u.id
            LEFT JOIN AppBundle:Post p 
            WITH c.postId = p.idp
            ORDER BY c.date DESC");

        return $this -> render('components/comment-feed-index.html.twig', [
            "rComments" => $comments->getResult()
        ]);
    }
}

