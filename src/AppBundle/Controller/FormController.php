<?php
/**
 * Created by PhpStorm.
 * User: Alan
 * Date: 14-Dec-16
 * Time: 6:34 PM
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use AppBundle\Entity\Post;
use AppBundle\Entity\UserSettings;
use AppBundle\Entity\Comment;

class FormController extends Controller
{
    /**
     * @Route ("/add-post", name="add-post")
     */
    public function addPostAction(Request $request)
    {
        //TODO: IMPORTANT: ESCAPE SPECIAL CHARACTERS
        //TODO: IMPORTANT: ADD UTF-8 ENCODING
        $post = new Post;
        $form = $this->createFormBuilder($post)
            ->add('Name', TextType::class)
            ->add('Text', TextareaType::class)
            ->add('save', SubmitType::class, array('label' => 'Create Post!'))
            ->getForm();


        $form->handleRequest($request);
        if ($form -> isSubmitted() && $form->isValid())
        {
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $post -> setIda($user->getId());
            $post = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();
            return $this->redirectToRoute("post", array('idp' => $post->getIdp()));
        }

        return $this -> render('default/add-post.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route ("/user-settings", name="user-settings")
     */
    public function UserSettingsAction (Request $request)
    {

        //get current user id
        $curUser = $this->getUser();
        $curId = $curUser->getId();
        //get current user information
        $em = $this->getDoctrine()->getManager();
        $userQuery = $em->createQuery("SELECT u FROM AppBundle:User u WHERE u.id = {$curId}");
        $userData = $userQuery->getResult();

        //Create a form with such values in place
        $userSettings = new UserSettings();
        $form = $this->createFormBuilder($userSettings)
            ->add('newName', TextType::class, array(
                'required' => false,
                'label' => "Change UserName",
                'data' => $userData[0] ->getUserName(),
                 'attr' => array(
                        'placeholder' => $userData[0]->getUsername()
                    )
            ))
            ->add('newMail', TextType::class, array(
                'required' => false,
                'label' => "Change Email",
                'data' => $userData[0] ->getEmail(),
                'attr' => array(
                    'placeholder' => $userData[0]->getEmail()
                )
            ))
            ->add('newPassword', PasswordType::class, array(
                'required' => false,
                'label' => "Change Password"
            ))
            ->add('currentPassword', PasswordType::class, array(
                'label' => "Current Password"
            ))
            ->add('save', SubmitType::class, array('label' => 'Update Information!'))
            ->getForm();

        $form->handleRequest($request);

        if ($form -> isSubmitted() && $form->isValid())
        {
            /**
             * 1. Check if current password is correct
             */
            $user_manager = $this->get('fos_user.user_manager');
            $factory = $this->get('security.encoder_factory');
            $user = $user_manager->findUserBy(array('id'=>$curId));
            $encoder = $factory->getEncoder($user);
            $bool = (
                $encoder->isPasswordValid($user->getPassword(),
                    $userSettings->getCurrentPassword(),
                    $user->getSalt())) ? "true" : "false";

            /**
             * 2. Update database if works, send back if not
             */
            if($bool){
                // Get data from post
                $post = $form->getData();
                // Store it in variables
                $name = $post->getNewName();
                $mail = $post->getNewMail();
                $pass = $post->getNewPassword();

                // Check if name or mail not occupied
                //TODO: MAKE FORM ERRORS APPEAR DIRECTLY IN THE FORM INSIDE PLACEHOLDERS
                if(isset($name) && $user_manager->findUserByEmail($mail)){
                   return $this->redirectToRoute("message", array('string' =>  "Email already Taken"));
                }
                elseif(isset($name) && $user_manager->findUserByUsername($name)){
                    return $this->redirectToRoute("message", array('string' =>  "User name already Taken!"));
                }
                else {
                    //Assign update variables
                    if ($name) {
                        $user->setUsername($name);
                    }
                    if ($mail) {
                        $user->setEmail($mail);
                    }
                    if ($pass) {
                        $user->setPlainPassword($pass);
                    }

                    $user_manager->updateUser($user);
                    return $this->redirectToRoute("user", array('id' => $curId));
                }
            }else{
                return $this->redirectToRoute("message", array('string' =>  "Invalid password"));
            }

        }
        return $this -> render('user/settings.html.twig', [
            "form" => $form -> createView()
        ]);
    }


    /**
     * * * * * * * * * * * * * * * * * * * *
     *    ~ POST HANDLING STARTS HERE ~    *
     * * * * * * * * * * * * * * * * * * * *
     */
    /**
     * @Route ("/post/{idp}", name="post")
     */
    public function postAction(Request $request, $idp)
    {

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery("SELECT p.idp, p.name, p.data, p.text, p.ida, u.username as author FROM AppBundle:Post p LEFT JOIN AppBundle:User u WITH p.ida = u.id WHERE p.idp = {$idp}");

        $comments = $em->createQuery("
            SELECT c.text, c.date, c.postId, u.username, u.id 
            FROM AppBundle:Comment c 
            LEFT JOIN AppBundle:User u 
            WITH c.authorId = u.id
            WHERE c.postId = {$idp} 
            ORDER BY c.date DESC");

        $newComment = new Comment;
        $form = $this->createFormBuilder($newComment)
            ->add('Text', TextareaType::class, array(
                'label' => false,
                'attr' => array(
                    'placeholder' => "I think that...."
                )
            ))
            ->add('save', SubmitType::class, array('label' => 'Add Comment!'))
            ->getForm();


        $form->handleRequest($request);
        if ($form -> isSubmitted() && $form->isValid())
        {
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $newComment -> setAuthorId($user->getId());
            $newComment -> setPostId($idp);
            $newComment = $form->getData();

            $em->persist($newComment);
            $em->flush();
            return $this->redirectToRoute("post", array('idp' => $idp));
        }

        return $this->render('post/post.html.twig', [
            'query' => $query->getResult(),
            'comments' => $comments->getResult(),
            "form" => $form -> createView()
        ]);
    }

    /**
     * @Route ("post/{id}/edit", name="edit")
     */
    public function postEditAction (Request $request, $id)
    {
        $em = $this -> getDoctrine() -> getManager();
        $postData = $em -> createQuery("SELECT p.name, p.text, p.ida FROM AppBundle:Post p WHERE p.idp = {$id}")
            ->getResult();

        $post = new Post;
        $form = $this->createFormBuilder($post)
            ->add('Name', TextType::class, array(
                'data' => $postData[0]['name']
            ))
            ->add('Text', TextareaType::class, array(
                'data' => $postData[0]['text']
            ))
            ->add('save', SubmitType::class, array('label' => 'Create Post!'))
            ->getForm();

        $form -> handleRequest($request);
        if ($form -> isSubmitted() && $form->isValid())
        {
            $data = $form -> getData();
            $update = $em -> createQuery("
              UPDATE AppBundle:Post p 
              SET p.text='{$data->getText()}', p.name = '{$data->getName()}'
              WHERE p.idp = {$id}");
            $update -> execute();

            return $this->redirectToRoute("post", array('idp' => $id));
        }


        return $this->render('post/post-edit.html.twig', [
            'form' => $form->createView(),
            'name' => $postData[0]['name'],
            'ida' => $postData[0]['ida'],
            'id' => $id
        ]);
    }

    /**
     * @Route ("post/{id}/delete", name="delete")
     */
    public function postDeleteAction (Request $request, $id)
    {
        $em = $this -> getDoctrine() -> getManager();
        $query = $em -> createQuery("SELECT p.name, p.idp, p.ida FROM AppBundle:Post p WHERE p.idp = {$id}");
        $submit = $this -> createFormBuilder()
            ->add('save', SubmitType::class, array('label' => 'Yup! Burn it to the ground! >:D'))
            ->getForm();

        $submit -> handleRequest($request);
        if($submit->isSubmitted() && $submit->isValid()){

            // 1. Remove post itself
            $post = $em -> getReference('AppBundle:Post', $id);
            $em->remove($post);
            $em->flush();

            $comments = $em -> createQuery("DELETE FROM AppBundle:Comment c WHERE c.postId = {$id}");
            $comments -> execute();

            return $this -> redirectToRoute("message", array('string' => "Post deleted! you monster"));

        }

        return $this-> render('post/post-delete.html.twig', [
            'post' => $query->getResult(),
            'form' => $submit->createView()
        ]);
    }

    public function searchFormAction()
    {
        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('search'))
            ->setMethod('GET')
            ->add("value", TextType::class, array('label' => false))
            ->add("Search", SubmitType::class)
            ->getForm();

        return $this->render('components/search-form.html.twig', [
            "form" => $form->createView()
        ]);
    }

    /**
     * @Route("/search", name="search")
     */
    public function searchAction(Request $request)
    {
        $value = $request->query->get('form')['value'];

        $em = $this -> getDoctrine() -> getManager();

        $query = $em -> createQuery("
        SELECT p.idp, p.name, p.data, SUBSTRING(p.text, 1, 150) as text, u.username as author 
        FROM AppBundle:Post p LEFT JOIN AppBundle:User u WITH p.ida = u.id
        WHERE p.name LIKE '%{$value}%' 
         ORDER BY p.idp DESC");
        return $this -> render ("post/post-search.html.twig", [
            "value" =>  $value,
            "query" => $query->getResult()
        ]);
    }
}