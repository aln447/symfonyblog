<?php
/**
 * Created by PhpStorm.
 * User: Alan
 * Date: 17-Dec-16
 * Time: 5:15 PM
 */

namespace AppBundle\Entity;

use AppBundle\Entity\User;

class UserSettings
{
    protected $newName;
    protected $newMail;
    protected $newPassword;
    protected $currentPassword;

    /**
     * @return mixed
     */
    public function getNewName()
    {
        return $this->newName;
    }

    /**
     * @return mixed
     */
    public function getNewMail()
    {
        return $this->newMail;
    }

    /**
     * @return mixed
     */
    public function getNewPassword()
    {
        return $this->newPassword;
    }

    /**
     * @return mixed
     */
    public function getCurrentPassword()
    {
        return $this->currentPassword;
    }

    /**
     * @param mixed $newName
     */
    public function setNewName($newName)
    {
        $this->newName = $newName;
    }

    /**
     * @param mixed $newMail
     */
    public function setNewMail($newMail)
    {
        $this->newMail = $newMail;
    }

    /**
     * @param mixed $newPassword
     */
    public function setNewPassword($newPassword)
    {
        $this->newPassword = $newPassword;
    }

    /**
     * @param mixed $currentPassword
     */
    public function setCurrentPassword($currentPassword)
    {
        $this->currentPassword = $currentPassword;
    }


}