<?php
namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="users")
 * @ORM\Entity
 */
class User extends BaseUser
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     *
     */
    protected $join_date;

    /**
     * Get idu
     *
     * @return integer
     */
    public function getIdu()
    {
        return $this->idu;
    }



    /**

    /**
     * Get joinDate
     *
     * @return string
     */
    public function getJoinDate()
    {
        return $this->join_date;
    }


}
