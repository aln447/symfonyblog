<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Table(name="post")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\PostRepository")
*/
class Post
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idp;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $ida;
    /**
     * @ORM\Column(type="string", length=350)
     */
    private $text;
    /**
     * @ORM\Column(type="string", length=30)
     */
    private $data;

    /**
     * Get idp
     *
     * @return integer
     */
    public function getIdp()
    {
        return $this->idp;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Post
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set ida
     *
     * @param integer $ida
     *
     * @return Post
     */
    public function setIda($ida)
    {
        $this->ida = $ida;

        return $this;
    }

    /**
     * Get ida
     *
     * @return integer
     */
    public function getIda()
    {
        return $this->ida;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return Post
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set data
     *
     * @param string $data
     *
     * @return Post
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

}
