<?php

namespace AppBundle\Entity\Repository;
use Doctrine\ORM\EntityRepository;

class PostRepository extends EntityRepository
{
    public function findAll()
    {
        return $this->findBy(array(), array('idp' => 'DESC'));
    }

    public function getIndexPostList()
    {
        /*
         * TODO: Add Pages Support
         */

    }
}